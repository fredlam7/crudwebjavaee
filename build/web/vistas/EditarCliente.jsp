<%-- 
    Document   : EditarCliente
    Created on : 19/09/2019, 12:28:17 PM
    Author     : Alfredo Lara
--%>

<%@page import="Model.Cliente"%>
<%@page import="ModelDao.ClienteDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Editar Cliente</h1>
        
        <%
            ClienteDAO dao = new ClienteDAO();
            int id = Integer.parseInt((String) request.getAttribute("idCliente"));
            Cliente c = (Cliente) dao.list(id);
        %>
        <table style="width:30%" border="1">
            <form action="Controlador">
                
            <tbody>
                <tr>
                    <td>Nombre</td>
                    <td ><input style="width:99%" type="text" name="txtName" value="<%= c.getNombre()%>"></td>
                </tr>
                <tr>
                    <td>Domicilio</td>
                    <td><input style="width:99%"  type="text" name="txtDom" value="<%= c.getDomicilio()%>"></td>
                    <input type="hidden" name="txtId" value="<%= c.getId()%>">
                </tr>
                <tr>
                    <td><input style="width:100%"  type="submit" name="accion" value="Actualizar"></td>
                    <td></td>
                </tr>
            </tbody>
            </form>
        </table>
    </body>
</html>
