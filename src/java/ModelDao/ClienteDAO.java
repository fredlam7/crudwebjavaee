/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import Config.Conexion;
import Interfaces.CRUD;
import Model.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alfredo Lara
 */
public class ClienteDAO implements CRUD{
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Cliente c = new Cliente();
    @Override
    public List listar() {
        ArrayList<Cliente> list = new ArrayList<>();
        String sql = "SELECT * FROM clientes";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                Cliente cl = new Cliente();
                cl.setId(rs.getInt("Clave_Cliente"));
                cl.setDomicilio(rs.getString("Domicilio"));
                cl.setNombre(rs.getString("Nombre"));
                list.add(cl);
                        
            }
        }catch(Exception ex){
            
        }
        return list;
    }

    @Override
    public Cliente list(int id) {
        String sql = "SELECT * FROM clientes WHERE Clave_Cliente="+id;
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                c.setId(rs.getInt("Clave_Cliente"));
                c.setDomicilio(rs.getString("Domicilio"));
                c.setNombre(rs.getString("Nombre"));
                
                        
            }
        }catch(Exception ex){
            
        }
        return c;
    }

    @Override
    public boolean add(Cliente c) {
        
        String sql = "INSERT INTO clientes(Nombre,Domicilio) VALUES('"+c.getNombre()+"','"+c.getDomicilio()+"');";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        }catch(Exception ex){
            
        }
        return false;
    }

    @Override
    public boolean edit(Cliente c) {
        String sql = "UPDATE clientes SET Nombre = '"+c.getNombre()+"',Domicilio = '"+c.getDomicilio()+"' WHERE Clave_Cliente="+c.getId();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean eliminar(int id) {
        String sql = "DELETE FROM clientes WHERE Clave_Cliente ="+id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
}
