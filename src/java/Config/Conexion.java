/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
    Connection con;
     
    public final String user = "root";
    public final String pass = "";
    public final String url = "jdbc:mariadb://localhost/";
    public final String db = "web";
    public Conexion()
    {
         try{
             Class.forName("org.mariadb.jdbc.Driver");
             con = DriverManager.getConnection(url+db,user,pass);
         }catch(Exception e){
             
         }
    }
    
    public Connection getConnection(){
        return con;
    }
}
