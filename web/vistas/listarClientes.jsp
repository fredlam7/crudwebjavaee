<%-- 
    Document   : listarClientes
    Created on : 19/09/2019, 10:52:58 AM
    Author     : Alfredo Lara
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ModelDao.ClienteDAO"%>
<%@page import="Model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Clientes</h1>
        <table style="width: 20%" border="1">
            <tbody>
                <tr>
                    <td style="width: 50%"><a href="Controlador?accion=add"><button style="width: 100%">Agregar Nuevo</button></a></td>
                    
                </tr>
            </tbody>
        </table>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Domicilio</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <%
                ClienteDAO dao = new ClienteDAO();
                List<Cliente> list= dao.listar();
                Iterator<Cliente> iter = list.iterator();
                Cliente c = null;
                while(iter.hasNext()){
                    c=iter.next();
            %>
            <tbody>
                <tr>
                    <td><%=c.getId()%></td>
                    <td><%=c.getNombre()%></td>
                    <td><%=c.getDomicilio()%></td>
                    <td>
                        <a href="Controlador?accion=editar&id=<%=c.getId()%>"><button>Editar</button></a>
                        <a href="Controlador?accion=eliminar&id=<%=c.getId()%>"><button>Remover</button></a>
                    </td>
                    
                    
                </tr>
                <%}%>
            </tbody>
            
        </table>

    </body>
</html>
